const env = process.env.NODE_ENV || 'development';
const dbConfig = require('./config.json')[env];
const Sequelize = require('sequelize');

// Connect to Database
const db = new Sequelize(dbConfig.database, dbConfig.username, dbConfig.password, {
    host: dbConfig.host,
    dialect: dbConfig.dialect
});

module.exports = db;