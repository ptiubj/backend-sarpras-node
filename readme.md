# Node.js
This project create using Node.js
Node.js is a JavaScript runtime built on Chrome's V8 JavaScript engine. For more information on using Node.js, see the [Node.js Website.](https://nodejs.org). 

## Official Node.js Documentation
[Node.js Documentation](https://nodejs.org/en/docs/)


# Express
Fast, unopinionated, minimalist web framework for [node](https://nodejs.org).

## Installation
This is a Node.js module available through the npm registry.

Before installing, [download and install Node.js](https://nodejs.org). Node.js 0.10 or higher is required.

Installation is done using the [npm install command:](https://docs.npmjs.com/getting-started/installing-npm-packages-locally)

`$ npm install express`

# Sequelize
This database orm using sequelize

Sequelize is a promise-based Node.js ORM for Postgres, MySQL, MariaDB, SQLite and Microsoft SQL Server. It features solid transaction support, relations, eager and lazy loading, read replication and more.

Sequelize follows [SEMVER](http://semver.org/). Supports Node v6 and above to use ES6 features.

New to Sequelize? Take a look at the [Tutorials and Guides](http://docs.sequelizejs.com/). You might also be interested in the [API Reference](http://docs.sequelizejs.com/identifiers).

## v5 Release
You can find the upgrade guide and changelog [here](http://docs.sequelizejs.com/manual/upgrade-to-v5.html). 

## Installation
`$ npm install --save sequelize # This will install v5`
`# And one of the following:`
`$ npm install --save pg pg-hstore # Postgres`
`$ npm install --save mysql2`
`$ npm install --save mariadb`
`$ npm install --save sqlite3`
`$ npm install --save tedious # Microsoft SQL Server`

### Tools
[Sequelize CLI](https://github.com/sequelize/cli)


# Installation this project

1. Create Database (current project using mysql database)
2. Setting Database in file config/config.json (development environment)
3. Install Dependencies
    * `$ npm install -g sequelize-cli`
    * `$ npm install`
    * `$ sequelize db:migrate`
    * `$ sequelize db:seed:all`
4. Start app using `npm run dev`
