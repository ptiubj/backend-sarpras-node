// load express module
const express = require('express');
// load body-parser for parsing json
const bodyParser = require('body-parser');
// load config database
const db = require('./config/database.js');
// load cors
const cors = require('cors');



const app = express();

// Parsing Data From Request
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

// Enabling CORS
app.use(cors());

db.authenticate()
    .then(() => {
        console.log(`Connection to ${db.config.database}  has been established successfully.`);
    })
    .catch(err => {
        console.error(`Unable to connect to the database ${db.config.database}:`, err);
    });

// routes
const categories = require('./routes/categories');

app.use('/categories', categories);


app.get('/', (req, res) => {
    res.json(['index']);
})

const PORT = process.env.PORT || 5000;

app.listen(PORT, console.log(`Server listen on http://localhost:${PORT}`));