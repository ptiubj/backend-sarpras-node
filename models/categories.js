'use strict';

module.exports = (sequelize, DataTypes) => {
  const Categories = sequelize.define('Categories', {
    name: DataTypes.STRING
  }, {
    freezeTableName: true
  });
  Categories.associate = function(models) {
    // associations can be defined here
  };
  return Categories;
};