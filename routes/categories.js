const express = require('express');
const router = express.Router();
const models = require('../models/index');

const { check, validationResult } = require('express-validator');

//middleware to hanlde errors 
const awaitErorrHandlerFactory = middleware => {
    return async (req, res, next) => {
        try {
            await middleware(req, res, next);
        } catch (err) {
            next(err);
        }
    };
};


router.get('/', (req, res) => {
    res.json([
        'categories'
    ]);
});

//Get All Data
router.get('/all', async (req, res, next) => {
    try {
        const categories = await models.Categories.findAll({});
        res.status(200).json(categories)
    } catch (err) {
        console.error('Error on ', err)
        res.status(400).json({ error: err});
    }
})

//Post New Data to Database
router.post('/add', [
        check('name', 'Name is required').not().isEmpty()
    ], async (req, res) => {
        const errors = validationResult(req);
        
        if(!errors.isEmpty()){
            res.status(422).json({
                'msg': errors.array()[0].msg
            });
        }
        
        try {
            const categories = await models.Categories.create(req.body);
            res.status(201).json(categories);
        } catch (err) {
            res.json({error: err});
        }

});

//Get Data By Id
router.get('/:id/edit', async (req, res) => {

    try {
        const categories = await models.Categories.findByPk(req.params.id);
        if(categories == null) {
            res.json({
                msg: 'Category not found',
            });
        }
        res.json(categories);
    } catch (err) {
        res.json({ error: err });
        
    }
    
});

//Update Data in Database
router.put('/:id/update',[
    check('name', 'Name is required').not().isEmpty()
    ], async (req, res) => {
        const errors = validationResult(req);
        
        if (!errors.isEmpty()) {
            return res.status(422).json({
                'state': false,
                'msg': errors.array()[0].msg
            });
        }
        
        let primaryKey = req.params.id;
        let dataUpdate = req.body;
        
        try {
            let categories = await models.Categories.findByPk(primaryKey);

            if (categories == null) {
                res.send(404).json({
                    'msg': 'Category Not Found'
                });
            }
            categories = await categories.update(dataUpdate);
            res.json(categories);

        } catch (error) {
            res.json({ error: err });
        }
    });
    
// Delete Data from database
router.delete('/:id/delete', async (req, res) => {
    let primaryKey = req.params.id;

    try {
        let categories = await models.Categories.findByPk(primaryKey);
        
        if (categories == null) {
            res.send(404).json({
                'msg': 'Category Not Found'
            });
        }

        categories = await categories.destroy();

        res.status(200).json({msg:'Category Successfull Deleted'});
    } catch (error) {
        res.json({ error: err });
    }
});

module.exports = router;